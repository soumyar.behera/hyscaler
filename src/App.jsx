import React from "react";
import Heading from "./Heading";
import Para from "./Para";
import Images from './Images';
import Greet from './Greet';

function App() {
    return (
        <>
            <Heading />
            <Para />
            <Images />
            <Greet />
        </>
    );
}
export default App;