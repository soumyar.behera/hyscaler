import React from "react";

var curDate = new Date(2023, 6, 1, 12);
let greeting = '';
const greetStyle = { color: 'green' };
if (curDate.getHours() >= 1 && curDate.getHours() < 12) {
    greeting = 'Good Morning';
    greetStyle.color = 'green';
} else if (curDate.getHours() >= 12 && curDate.getHours() < 19) {
    greeting = 'Good Afternoon';
    greetStyle.color = 'orange';
} else {
    greeting = 'Good Night';
    greetStyle.color = 'black';
}

function Greet() {
    return (
        <div className="greet">
            <h1>Hello sir, <span style={greetStyle}>{greeting}</span></h1>
        </div>
    )
}

export default Greet;