import React from "react";

function Images() {
    return (
        <div className="imgdiv">
            <img src="https://picsum.photos/200" alt="Image" />
            <img src="https://picsum.photos/250" alt="Image" />
            <img src="https://picsum.photos/230" alt="Image" />
        </div>
    );
}

export default Images;