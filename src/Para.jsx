import React from "react";

const fname = 'Soumya Ranjan'
const lname = 'Behera'
var curDate = new Date();
var curTime = new Date().toLocaleTimeString();


const paragraph = {
    color: 'rgb(76, 97, 124)',
    textTransform: 'capitalize'
}

function Para() {
    return (
        <div>
            <p style={paragraph}>{`My name is ${fname} ${lname}`}</p>
            <p>Current Date: {curDate.toLocaleDateString()}</p>
            <p>Current Time: {curTime}</p>
        </div>
    );
}

export default Para;